import re
from models.model import *

class Session:
    user_id = 0
    access = 0;


class LogInController:

    @staticmethod
    def is_user_active(username):

        for customer in customers:
            if customer.username == username and customer.active is True:
                return True
        for staff in stafflist:
            if staff.username == username and staff.active is True:
                return True
        for admin in admins:
            if admin.username == username and admin.active is True:
                return True

        return False

    @staticmethod
    def attempt_log_in(username, password):

        for customer in customers:
            if customer.username == username and customer.password_hash == password:
                return customer.user_id
        for staff in stafflist:
            if staff.username == username and staff.password_hash == password:
                return staff.user_id
        for admin in admins:
            if admin.username == username and admin.password_hash == password:
                return admin.user_id
        return None

    @staticmethod
    def get_user_access(id,screen):
        for customer in customers:
            if customer.user_id == id:
                screen.title("Rental Management System - Customer")
                return 0
        for staff in stafflist:
            if staff.user_id == id:
                screen.title("Rental Management System - Staff")
                return 1
        for admin in admins:
            if admin.user_id == id:
                screen.title("Rental Management System - Admin")
                return 2
        return None


class RegisterUserController:
    @staticmethod
    def register_new_user(full_name, email, region, username, password):
        for customer in customers:
            print(customer.username + " " + customer.email)

        new_user = Customer(full_name, username, password, email, region, 111111111111111, datetime.date(2020, 2, 1), 111)

        customers.append(new_user)
        for customer in customers:
            print(customer.username + " " + customer.email)
        return True



class RegisterStaffController:
    @staticmethod
    def register_staff(full_name, region, email, username, password, cap):
        new_staff = Staff(full_name, username, password, email, region, cap)
        stafflist.append(new_staff)
        JobListController.new_joblist(new_staff.user_id)

        for s in stafflist:
            print(s.username)

        return True


# User Controller (Possibly merge register user into this)
class UserController:
    def get_user_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u
        for u in stafflist:
            if u.user_id == id:
                return u
        for u in admins:
            if u.user_id == id:
                return u
        return None

    def get_user_cardlongnumber_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u.cardLongNumber
        return None

    def get_user_cardexpirydate_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u.cardExpiryDate
        return None

    def get_user_cardcvv_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u.cardCVV
        return None

    def get_user_name_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u.name
        for s in stafflist:
            if s.user_id == id:
                return s.name
        for a in admins:
            if a.user_id == id:
                return a.name
        return None

    def get_region_from_userid(id):
        for u in customers:
            if u.user_id == id:
                return u.region
        for s in stafflist:
            if s.user_id == id:
                return s.region
        for a in admins:
            if a.user_id == id:
                return a.region


    # Returns all customers
    @staticmethod
    def get_all_customers():
        return customers

    # Returns all staff
    @staticmethod
    def get_all_staff():
        return stafflist

    # Returns all admin
    @staticmethod
    def get_all_admin():
        return admins

        # Checks username against existing username's, returns false if username is not unique

    @staticmethod
    def validate_user_name(username):
        for customer in customers:
            if customer.username == username:
                return False
        for staff in stafflist:
            if staff.username == username:
                return False
        for admin in admins:
            if admin.username == username:
                return False
        return True

    @staticmethod
    def validate_email(email):
        for customer in customers:
            if customer.email == email:
                return False
        for staff in stafflist:
            if staff.email == email:
                return False
        for admin in admins:
            if admin.email == email:
                return False
        return True

    # checks email format
    @staticmethod
    def validate_email_format(email):
        if re.search(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
            print("email is correct format")
            return True

        return False
#Category Controller
class CategoryController:

    def getCategories(self):
        return categories

    def getTitleFromCategoryID(self, id):
        for cat in categories:
            if cat.category_id == id:
                return cat.title
    def doesCategoryExist(self, category):
        for cat in categories:
            if cat.title == category.title:
                return True
        return False

    def addCategory(self, title, desc):
        new_cat = Category(title,desc)
        if not self.doesCategoryExist(self,new_cat):
            categories.append(new_cat)
            return True
        else:
            return False


# ITEM CONTROLLER
class ItemController:

    def get_items_from_category(self, item_category):
        items_from_category = []
        for i in items:
            if i.category == item_category:
                items_from_category.append(i)
        return items_from_category

    def get_items_from_search_term(self, search_term):
        query_items = []
        search_term = search_term.lower()
        cat_controller = CategoryController
        for i in items:
            if i.title.lower().find(search_term) != -1 or cat_controller.getTitleFromCategoryID(cat_controller,i.category_id).lower().find(search_term) != -1:
                query_items.append(i)
        return query_items

    def get_item_by_id(self, item_id):
        for i in items:
            if i.item_id == item_id:
                return i

    def get_items(self):
        return items

    def add_item_info(self, title, cost_per_day, owner_id, category, available, region, info):
        cat = self.get_categoryID_from_title(category)
        new_item = Item(title, cost_per_day, owner_id, cat, available, region, info)
        if not self.check_item_exist(new_item):
            items.append(new_item)
            return True
        else:
            return False

    def check_item_exist(self, item):
        items = self.get_items()
        for i in items:
            if(i.item_id == item.item_id):
                return True
        return False

    def get_categories(self):
        all_categories = []
        for category in categories:
            all_categories.append(category.title)
        return all_categories

    def get_categoryID_from_title(self, name):
        for cat in categories:
            if(cat.title == name):
                return cat.category_id
        return None

# Transaction Controller
class TransactionController:
    def get_transaction_from_id(transaction_id):
        for t in transactions:
            if t.transaction_id == transaction_id:
                return t
        return None

    def get_transactions_from_userid(userid):
        toReturn = []
        for t in transactions:
            if t.renter_id == userid:
                toReturn.append(t)
        return toReturn

    def get_transactions_from_query(query, userid):
        i_controller = ItemController
        u_controller = UserController

        toReturn = []
        q = query.lower()
        for t in transactions:
            print(str(t.renter_id) + str(userid))
            if t.renter_id is not userid:
                continue
            if (i_controller.get_item_by_id(i_controller, t.item_id).title.lower().find(q) != -1 or  # query item title
                    i_controller.get_item_by_id(i_controller, t.item_id).category.lower().find(
                        q) != -1 or  # query item category
                    u_controller.get_user_from_userid(t.owner_id).name.lower().find(q) != -1  # query person rented from
            ):
                toReturn.append(t)
        return toReturn

    def order_details_from_transaction(transaction):
        i_controller = ItemController
        u_controller = UserController
        # order : [object title, rented from, rented until, total cost]
        order = []
        item = i_controller.get_item_by_id(i_controller.get_item_by_id, transaction.item_id)
        owner = u_controller.get_user_from_userid(transaction.owner_id)
        order.append(item.title)
        order.append(transaction.startDate)
        order.append(transaction.endDate)
        order.append(owner.name)
        time_rented = (transaction.endDate - transaction.startDate)
        price = time_rented.days * item.cost_per_day
        order.append(price)

        return order

    @staticmethod
    def has_review(transaction_id):
        for review in reviews:
            if review.transaction_id == transaction_id:
                return True
        return False

    def create_new_transaction(self, transaction_id, item_id, owner, renter, total_cost, start_date, end_date):
        # create new Transaction object
        new_t = Transaction(transaction_id, item_id, owner, renter, total_cost, start_date, end_date)

        # add to Transactions list in model
        transactions.append(new_t)

        # debug - print all transactions
        for t in transactions:
            print(vars(t))

    def get_new_transaction_id(self):
        new_t_id = 0
        for t in transactions:
            if t.transaction_id > new_t_id:
                new_t_id = t.transaction_id

        # increment transaction id so it is unique
        new_t_id = new_t_id + 1

        return new_t_id

# ALL JOBS CONTROLLER
class JobsController:
    # Returns a job from provided job id
    def get_job_from_jobid(job_id):
        for j in jobs:
            if (j.job_id == job_id):
                return j
        return None

    # Returns a joblist for given staffID
    def get_joblist_from_staff_id(staff_id):
        for j in jobLists:
            if (j.staff_id == staff_id):
                return j

    # returns jobs from given joblist
    def get_jobs_from_joblist(j_listid):
        # array to store all jobs related to this staff
        staff_jobs = []
        for job in jobs:
            # if jobsID = joblistID
            if (job.job_list_id == j_listid):
                # append the job to the array of jobs
                staff_jobs.append(job)
        # return the array of all jobs related to that joblist
        if staff_jobs == None:
            print("no jobs!!!!!")
        return staff_jobs

    def get_unallocated_jobs(self):
        incomplete_jobs = []
        for job in jobs:
            if job.job_list_id is None:
                incomplete_jobs.append(job)
        return incomplete_jobs

    # completes a job
    def complete_job(job):
        # check job exists
        for j in jobs:
            if j == job:
                j.delivered_datetime = datetime.datetime.now()
                return True
        return False

    def create_new_job(self, transaction_id):
        next_job_id = 0
        for job in jobs:
            if job.job_id > next_job_id:
                next_job_id = job.job_id
        next_job_id += 1
        new_job = Job(next_job_id,transaction_id,None,None)
        jobs.append(new_job)



    def get_staff_from_joblist(joblist):
        for staff in stafflist:
            if staff.user_id == joblist.staff_id:
                return staff

    def count_jobs_associated_to_joblist(joblist):
        count = 0
        for job in jobs:
            if job.job_list_id == joblist.job_list_id:
                count += 1
        return count



    def get_cap_difference_from_joblist(self, joblist):
        stf = self.get_staff_from_joblist(joblist)
        capacity = stf.cap
        num_jobs_in_joblist = self.count_jobs_associated_to_joblist(joblist)
        return (capacity - num_jobs_in_joblist)



    def sort_collection_of_joblists_by_lowest_cap_difference(self,collection_of_joblists):
        ordered = False
        index = 0
        while not ordered and index<len(collection_of_joblists)-1:
            #print("JL1 : ",self.get_cap_difference_from_joblist(self,collection_of_joblists[index]),"     JL2:",self.get_cap_difference_from_joblist(self,collection_of_joblists[index+1]))
            if (self.get_cap_difference_from_joblist(self,collection_of_joblists[index])
                    > self.get_cap_difference_from_joblist(self,collection_of_joblists[index+1])):
                #print("SWITCH!")
                temp = collection_of_joblists[index]
                collection_of_joblists[index] = collection_of_joblists[index+1]
                collection_of_joblists[index+1] = temp
                #print("JL1 : ", self.get_cap_difference_from_joblist(self,collection_of_joblists[index]), "     JL2:",
                      #self.get_cap_difference_from_joblist(self,collection_of_joblists[index + 1]))
                index = 0
            elif index == len(collection_of_joblists)-2:
                ordered = True

            index += 1
        #for jl in collection_of_joblists:
            #print(jl.job_list_id)

    def find_job_region(job_id):
        return UserController.get_user_from_userid(TransactionController.get_transaction_from_id(JobsController.get_job_from_jobid(job_id)))

    def find_joblist_region(job_list_id):
        UserController.get_region_from_userid(JobListController.get_joblist_from_id(job_list_id).user_id)

    def allocate_unallocated_jobs(self):
        my_joblists = list.copy(jobLists) #Get all joblists
        unallocated_jobs = self.get_unallocated_jobs(self) #Get joblists without joblist_ids
        if len(unallocated_jobs) == 0: #If no unallocated jobs, return
            return False

        # Sort joblists in order of closest to filling capacity
        self.sort_collection_of_joblists_by_lowest_cap_difference(self,my_joblists)

        joblist_index = 0
        job_index = 0

        # while joblist is not empty and we are in bounds of job
        while len(my_joblists) > 0 and joblist_index < len(my_joblists):
            print("joblist_index: ", joblist_index, "/", len(my_joblists)," Space: ", self.get_cap_difference_from_joblist(self,my_joblists[joblist_index]))
            print("Staff: ",UserController.get_user_name_from_userid(my_joblists[joblist_index].staff_id),
                  " Region: ",UserController.get_region_from_userid(my_joblists[joblist_index].staff_id))

            job_index = 0
            # while there are unallocated jobs and we are in bounds
            while len(unallocated_jobs) > 0 and job_index < len(unallocated_jobs):
                print("     job_index: ",job_index,"/",len(unallocated_jobs),"Region: ",
                      UserController.get_region_from_userid(TransactionController.get_transaction_from_id(unallocated_jobs[job_index].transaction_id).owner_id), end="")
                #If job.region == joblist.region
                if (UserController.get_region_from_userid(TransactionController.get_transaction_from_id(unallocated_jobs[job_index].transaction_id).owner_id)
                    == UserController.get_region_from_userid(my_joblists[joblist_index].staff_id)):
                    # Assign job to joblist
                    unallocated_jobs[job_index].job_list_id = my_joblists[joblist_index].job_list_id
                    print("  < ------ Job assigned to joblist"," Space: ", self.get_cap_difference_from_joblist(self,my_joblists[joblist_index]), end="")
                    unallocated_jobs.pop(job_index)
                print("")
                job_index += 1
            joblist_index += 1


        print(self.get_unallocated_jobs(self))





# VIEW JOBS INFO CONTROLLER
class JobsInfoController:
    def get_all_jobs_info(self):
        # add total number of jobs to list
        jobs_info = []
        total_jobs = len(jobs)
        jobs_info.append(total_jobs)

        # add total number of completed jobs to list
        comp_jobs = []
        for j in jobs:
            # check for completed jobs
            if j.delivered_datetime is not None:
                comp_jobs.append(j)
        completed_num = len(comp_jobs)
        jobs_info.append(completed_num)

        # add total number of incomplete jobs to list
        incomp_jobs = []
        for j in jobs:
            # check for incomplete jobs
            if j.delivered_datetime is None:
                incomp_jobs.append(j)
        incomplete_num = len(incomp_jobs)
        jobs_info.append(incomplete_num)

        #calculate incomplete percentage
        incomp_percent = round(((incomplete_num / total_jobs) * 100), 2)
        jobs_info.append(incomp_percent)

        # return jobs info list
        return jobs_info

    def get_all_jobs_by_region_info(self, query):
        # get all jobs with region matching query
        regional_jobs = []
        for j in jobs:
            tid = j.transaction_id # get transaction id from job

            for t in transactions:
                if t.transaction_id == tid:
                    iid = t.item_id     # get item id from transaction
                    for i in items:
                        if i.item_id == iid:
                            if i.region == query:
                                regional_jobs.append(j)

        # add total number of jobs to list
        jobs_info = []
        total_jobs = len(regional_jobs)
        jobs_info.append(total_jobs)

        # add total number of completed jobs to list
        comp_jobs = []
        for j in regional_jobs:
            # check for completed jobs
            if j.delivered_datetime is not None:
                comp_jobs.append(j)
        completed_num = len(comp_jobs)
        jobs_info.append(completed_num)

        # add total number of incomplete jobs to list
        incomp_jobs = []
        for j in regional_jobs:
            # check for incomplete jobs
            if j.delivered_datetime is None:
                incomp_jobs.append(j)
        incomplete_num = len(incomp_jobs)
        jobs_info.append(incomplete_num)

        #calculate incomplete percentage
        incomp_percent = round(((incomplete_num / total_jobs) * 100), 2)
        jobs_info.append(incomp_percent)

        print(regional_jobs)

        # return jobs info list
        return jobs_info

class JobListController:
    def new_joblist(userID):
        new_jl = Joblist(userID)
        jobLists.append(new_jl)

    def get_joblist_from_id(id):
        for jl in jobLists:
            if jl.job_list_id == id:
                return jl

class ReviewController:
    def get_review_from_transaction_id(self, id):
        for review in reviews:
            if review.transaction_id == id:
                return review

    def create_new_review(self, transaction_id, content, rating):
        new_review = Review(transaction_id,content,rating)
        reviews.append(new_review)
        print("ADDED: ",transaction_id,content,rating)

    def edit_review(self, transaction_id, content, rating):
        review = self.get_review_from_transaction_id(self,transaction_id)
        review.content = content
        review.rating = rating
        print("EDITED: ",transaction_id,content,rating)




#jc = JobsController
# for jl in jobLists:
#     if jl.staff_id == 5:
#         print("Jez's jobs in Cambridge")
#         for job in jobs:
#             if job.job_list_id == jl.job_list_id:
#                 print(job.job_id)
#
#     if jl.staff_id == 6:
#         print("Norfolk:")
#         for job in jobs:
#             if job.job_list_id == jl.job_list_id:
#                 print(job.job_id)
#
# jc.allocate_jobs_to_drivers(jc, "Cambridgeshire")
#
# for jl in jobLists:
#     if jl.staff_id == 5:
#         print("Jez's jobs in Cambridge")
#         for job in jobs:
#             if job.job_list_id == jl.job_list_id:
#                 print(job.job_id)
#
#     if jl.staff_id == 6:
#         print("Norfolk:")
#         for job in jobs:
#             if job.job_list_id == jl.job_list_id:
#                 print(job.job_id)