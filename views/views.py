import tkinter as tk
from tkinter import *
from tkinter import messagebox
import controllers.controllers as ctrl
import sys, string, calendar
import time
from functools import partial
import datetime

# LOG IN ------------------------------
class LogInView:
    # Constructor
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen
        screen.title("Rental Management System")
        # Stores entered username and password
        self.username_attempt = StringVar()
        self.password_attempt = StringVar()

    # Attempts to log in user
    def log_in(self):
        print("Logging in...")

        # If log in details are correct log in
        if ctrl.LogInController.attempt_log_in(self.username_attempt.get(), self.password_attempt.get()) is not None:
            session.user_id = ctrl.LogInController.attempt_log_in(self.username_attempt.get(),
                                                                  self.password_attempt.get())
            session.access = ctrl.LogInController.get_user_access(session.user_id, screen)

            if ctrl.LogInController.is_user_active(self.username_attempt.get()):
                print("Log in successful")
                print("user is active")
                self.nav_to_nav_view()
            else:
                self.render("Account has been deactivated, please contact support - support@rental.com")
        else:
            print("Log in failed")
            self.render("Incorrect Username/password")

    # Navigates to register page
    def nav_to_register(self):
        # Initialise registration view and render
        reg_usr_view = RegisterUserView(self.screen)
        reg_usr_view.render("")

    # Navigates to main navigation view
    def nav_to_nav_view(self):
        nav_view = MainNavView(self.screen)
        nav_view.render()

    # Initialises and draws log in screen
    def render(self, message):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # SCREEN LAYOUT
        Label(screen, text="Log in", bg="grey", width="300", height="2", font=("calibri", 13)).pack()
        Label(screen, text="").pack()

        Label(screen, text="Username", font=("Calibri", 13)).pack()
        username_entry = Entry(screen, textvariable=self.username_attempt)
        username_entry.pack()

        Label(screen, text="Password", font=("Calibri", 13)).pack()
        password_entry = Entry(screen, show="*", textvariable=self.password_attempt)
        password_entry.pack()

        Label(screen, text="").pack()

        Button(screen, text="Login", height="2", width="30", command=lambda: self.log_in()).pack()
        Label(screen, text="").pack()
        Button(screen, text="Register", height="2", width="30", command=lambda: self.nav_to_register()).pack()

        Label(screen, text=message, fg="red",
              font=("calibri", 11)).pack()

        screen.mainloop()


# REGISTER -----------------------------
class RegisterUserView:
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen

        self.full_name = StringVar()
        self.email = StringVar()
        self.region = StringVar()
        self.username = StringVar()
        self.password = StringVar()

    # Navigates to log in screen
    def nav_to_log_in(self):
        # Initialise registration view and render
        log_in_view = LogInView(self.screen)
        log_in_view.render("")

    def register_user(self):
        # Check for empty fields
        if self.full_name.get() == "":
            self.render("Full name is required")
            return False
        if self.email.get() == "":
            self.render("Email address is required")
            return False
        if self.region.get() == "":
            self.render("Region is required")
            return False
        if self.username.get() == "":
            self.render("Username is required")
            return False
        if self.password.get() == "":
            self.render("Password is required")
            return False

        print("Registering...")
        if ctrl.UserController.validate_email_format(self.email.get()):

            # Check email is unique
            if ctrl.UserController.validate_email(self.email.get()):
                print("Email is unique")

                # Check username is unique
                if ctrl.UserController.validate_user_name(self.username.get()):
                    print("username is unique")

                    # If log in details are correct log in
                    if ctrl.RegisterUserController.register_new_user(self.full_name.get(),
                                                                     self.email.get(),
                                                                     self.region.get(),
                                                                     self.username.get(),
                                                                     self.password.get()):
                        print("Registration successful")
                        # Return to log in screen
                        self.nav_to_log_in()
                    else:
                        print("Registration failed")
                        self.render("Registration failed")
                else:
                    print("Username taken")
                    self.render("Username taken")
            else:
                self.render("Email already in use")

        else:
            print("Email is in wrong format")
            self.render("Please enter a valid email")

    # Initialises and draws log in screen
    def render(self, err_message):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # SCREEN LAYOUT
        Label(screen, text="Please enter details below", bg="grey", width="300", height="2",
              font=("Calibri", 13)).pack()
        Label(screen, text="").pack()

        Label(screen, text="Full name *").pack()
        reg_full_name_entry = Entry(screen, textvariable=self.full_name)
        reg_full_name_entry.pack()

        Label(screen, text="Email *").pack()
        reg_email_entry = Entry(screen, textvariable=self.email)
        reg_email_entry.pack()

        Label(screen, text="Region *").pack()
        reg_region_entry = Entry(screen, textvariable=self.region)
        reg_region_entry.pack()

        Label(screen, text="Username *").pack()
        reg_username_entry = Entry(screen, textvariable=self.username)
        reg_username_entry.pack()

        Label(screen, text="Password *").pack()
        password_entry = Entry(screen, textvariable=self.password)
        password_entry.pack()

        Button(screen, text="Register", width=10, height=1, command=lambda: self.register_user()).pack()
        Button(screen, text="Back", width=10, height=1, command=lambda: self.nav_to_log_in()).pack()
        Label(screen, text=err_message, fg="red", font=("calibri", 11)).pack()


# ADD ITEM VIEW ------------------------
class AddItemView:
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen
        self.id = StringVar()
        self.title = StringVar()
        self.cost_per_day = StringVar()
        self.owner_ID = StringVar()
        self.category = StringVar()
        self.region = StringVar()
        self.info = StringVar()

    # Adding a new item to the list of items
    def add_item_info(self, f2, f3, f4, f5, f8, frame):
        title = f2.get()
        cost_per_day = f3.get()
        owner_ID = session.user_id
        category = str(f5.get())
        available = True
        region = ctrl.UserController.get_region_from_userid(session.user_id)
        info = f8.get("1.0", END)

        item_control = ctrl.ItemController()
        if (f2.get() == ""):
            self.render("Please enter a title.")
        elif(f3.get()== ""):
            self.render("Please enter cost.")
        elif(not re.search(r"^([0-9]+)$", f3.get())):
            self.render("Cost must be a number")
        elif(f8.get("0.0" , END) == None):
            self.render("Please give short description.")
        else:
            # If the item is successfully created, print success
            if(item_control.add_item_info(title, cost_per_day, owner_ID, category, available, region, info)):
                f2.delete(0)
                f3.delete(0)
                f8.delete('1.0', END)
                self.render("Item added successfully")
            else:
                # else print fail.
                f2.delete(0)
                f3.delete(0)
                f8.delete('1.0', END)
                self.render("You can't add item with existing ID")
        # # clearing the entry fields.


    #To check if the entry is int
    def validate_entry(self, value):
        if value:
            v = int(value)
            return v
        else:
            return None

    def render(self, message):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # SCREEN LAYOUT
        Label(screen, text="Add new item", bg="grey", width="300", height="2", font=("calibri", 13)).pack()
        Label(screen, text="").pack()

        frame = Frame(screen, width = 600, height = 500)
        frame.place(relx = 0.375, rely=0.275)
        # Creating labels for fields
        Label(frame, text="Title").grid(row=1, column=0)
        Label(frame, text="Cost per day £").grid(row=2, column=0)
        Label(frame, text="OwnerID").grid(row=3, column=0)
        Label(frame, text="Category").grid(row=4, column=0)
        Label(frame, text="Short info").grid(row=6, column=0)

        #dropdown menu for categories
        category = ctrl.ItemController.get_categories(self)
        cat = StringVar(frame)
        cat.set(category[0])

        # Entry fields for user to input text

        f2 = Entry(frame, textvariable=self.title)
        f2.grid(row=1, column=1)
        f3 = Entry(frame, textvariable=self.cost_per_day)
        f3.grid(row=2, column=1)
        print(f3)
        f4 = Label(frame, text=session.user_id)
        f4.grid(row=3, column=1)
        f5 = OptionMenu(frame, cat, *category)
        f5.grid(row=4, column=1)
        f8 = Text(frame, height=3.5, width=20, font=("Courier", 8))
        f8.grid(row=6, column=1)

        if message == "Item added successfully":
            Label(frame, text=message, fg="green").grid(row=9, column = 0)
        else:
            Label(frame, text=message, fg="red").grid(row=9, column = 0)


        button = Button(frame, text="Add", command=lambda: self.add_item_info(f2,f3,session.user_id,cat, f8, frame))
        button.grid(row=12, column=6)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(frame, text="Go Back", command=lambda: main_nav_view.render())
        button.grid(row=12, column=9, pady=4)

        screen.mainloop()


# VIEW ITEMS ---------------------------
class ViewItems:
    d = 0
    p = 0

    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen

    def display_all_items(self, item_area):
        i_controller = ctrl.ItemController
        items = i_controller.get_items(i_controller)
        self.display_items(item_area, items, "")

    def display_queried_items(self, item_area, query):
        i_controller = ctrl.ItemController
        items = i_controller.get_items_from_search_term(i_controller, query)
        self.display_items(item_area, items, query)

    def display_items(self, item_area, items, items_group):

        for widget in item_area.winfo_children():
            widget.destroy()

        cat_controller = ctrl.CategoryController

        r = 0

        item_list = []

        print("Title", "Category")
        for item in items:
            # if show_availible.get() and not item.available:
            #     continue
            item_area.rowconfigure(r, weight=2)

            image = Label(item_area, text="IMAGE", bg="green")

            image.grid(row=r, column=0, padx=(50, 50))

            title = Label(item_area, text=item.title)

            item_list.append(item)
            if session.access == 0:
                title.bind("<Button-1>", partial(self.display_item_focused, item_area=item_area, item=item,
                                                 last_items_displayed=items_group))
            title.grid(row=r, column=1, padx=(50, 50))
            title.configure(font=("", 15))

            category = Label(item_area, text=cat_controller.getTitleFromCategoryID(cat_controller,item.category_id))
            category.grid(row=r, column=2, padx=(30, 30))
            category.configure(font=("", 15))

            price = Label(item_area, text="£"+ str(item.cost_per_day))
            price.grid(row=r, column=3, padx=(30, 30))
            price.configure(font=("", 15))

            info = Text(item_area, bg="#F0F0F0", wrap=WORD)
            info.insert(0.0, item.info)
            info.config(font=("", 8), width=30, bd=0, state=DISABLED)

            info.grid(row=r, column=4, padx=(30, 30), pady=(15,0))

            r += 1


    def back_to_items(self, button_to_destroy, last_items_displayed, item_area, al, a2, a3, a4, e1, e2, Rent,
                      Calc, T):
        global d
        for widget in item_area.winfo_children():
            widget.destroy()
        button_to_destroy.destroy()
        al.destroy()
        a2.destroy()
        a3.destroy()
        a4.destroy()
        e1.destroy()
        e2.destroy()
        T.destroy()
        Rent.destroy()
        Calc.destroy()
        d = None
        self.display_queried_items(item_area, last_items_displayed)

    def display_item_focused(self, event, item_area, item,
                             last_items_displayed):  # Display one item focused with information
        for widget in item_area.winfo_children():
            widget.destroy()

        cat_controller = ctrl.CategoryController

        # create button to return to item_view
        return_to_items = Button(screen, text="Go Back",
                                 command=lambda: self.back_to_items(return_to_items, last_items_displayed, item_area,
                                                                    a1, a2, a3, a4, e1, e2, Rent, Calc, T))
        return_to_items.place(relx=0.7, rely=0, relheight=0.1)

        # display item
        image = Label(item_area, text="IMAGE", bg="green", width=20, height=10)
        image.pack(side=LEFT)

        title = Label(item_area, text=item.title, font=44)
        title.pack()

        category = Label(item_area, text="Category: " + cat_controller.getTitleFromCategoryID(cat_controller, item.category_id))
        category.pack()

        price = Label(item_area, text="Cost (per day) £ : " + str(item.cost_per_day))
        price.pack()

        info = Text(item_area, bg="#F0F0F0", wrap=WORD, width=30, height=20)
        info.insert(0.0, item.info)
        info.config(font=("", 8), width=20, bd=0, state=DISABLED)
        info.pack()

        fnta = ("Times", 10)

        strdays = "Mon  Tue  Wed  Thu  Fri  Sat Sun"

        year = time.localtime()[0]
        month = time.localtime()[1]
        day = time.localtime()[2]
        day1 = time.localtime()[2] + 1
        strdate = (str(year) + "/" + str(month) + "/" + str(day))
        strdate1 = (str(year) + "/" + str(month) + "/" + str(day1))
        current = (str(year) + "/" + str(month) + "/" + str(day))


        # Method takes a month and returns the number of days thats supposed to be in that mont
        def day_of_month(m):
            if m == 1 or 3 or 5 or 7 or 8 or 10 or 12:
                numdays = 31
            elif m == 4 or 6 or 9 or 11:
                numdays = 30
            else:
                numdays = 28
            return numdays

        # Method calls a calendar class object
        def fnCalendar(date_var):
            parent = ()
            tkCalendar(parent, year, month, day, date_var)

        def date_to_days(date):
            dateSplit = date.split('/')
            numDays = int(dateSplit[0]) * 365 + int(dateSplit[1]) * day_of_month(dateSplit[1]) + int(dateSplit[2])
            return numDays

        # Method takes entry fields and converts the date into number of days
        def entry_to_days(entry):
            date = entry.get()
            dateSplit = date.split('/')
            numDays = int(dateSplit[0]) * 365 + int(dateSplit[1]) * day_of_month(dateSplit[1]) + int(dateSplit[2])
            return numDays

        # Method calculates how much customer needs to pay for item
        def calc():
            global d
            global p
            a = entry_to_days(e1)
            b = entry_to_days(e2)

            global rental_start_date
            rental_start_date = datetime.datetime.strptime(e1.get(), "%Y/%m/%d").date()

            global rental_end_date
            rental_end_date = datetime.datetime.strptime(e2.get(), "%Y/%m/%d").date()
            if entry_to_days(e1) < date_to_days(current):
                messagebox.showerror("Error", "Cannot pick past dates\nCurrent date " + current)
            else:
                days = b - a
                if days <= 0:
                    messagebox.showerror("Error", "Check dates \n Invalid dates")
                else:
                    print(item.cost_per_day)
                    print(days)
                    price = days * int(item.cost_per_day)
                    print(price)
                    T.delete('1.0', END)
                    T.insert(END, days)
                    T.insert(END, "days =")
                    T.insert(END, "£")
                    T.insert(END, price)
                    T.place(relx=0.62, rely=0.85, relwidth=0.15, relheight=0.05)
                    d = days
                    p = price

        def order_summary(a1, a2, a3, a4, e1, e2, Rent, Calc, T):
            global d
            try:
                d
            except NameError:
                d = None
            if d is None:
                messagebox.showerror("Error", "Calculate price first")
            else:
                a1.destroy()
                a2.destroy()
                a3.destroy()
                a4.destroy()
                e1.destroy()
                e2.destroy()
                T.destroy()
                Rent.destroy()
                Calc.destroy()
                begindate = Label(item_area, textvariable=date_var1)
                begindate.place(relx=0.50, rely=0.30)
                until = Label(item_area, text="until")
                until.place(relx=0.515, rely=0.34)
                enddate = Label(item_area, textvariable=date_var2)
                enddate.place(relx=0.50, rely=0.38)
                daystorent = Label(item_area, text="Days item will be rented for - " + str(d))
                daystorent.place(relx=0.43, rely=0.42)
                price = Label(item_area, text="To pay - £ " + str(p))
                price.place(relx=0.49, rely=0.46)
                b = Button(item_area, text='Proceed to payment', bg='#70bed2', relief=GROOVE, command=lambda: proceed_to_payment())
                b.place(relx=0.46, rely=0.55, relwidth=0.17, relheight=0.08)
                d = None

                # create new transaction
                # get latest transaction id
                new_t_id = ctrl.TransactionController.get_new_transaction_id(self)

                # call controller to add transaction
                # (needs these parameters: transaction_id, item_id, owner, renter, total_cost, start_date, end_date)
                ctrl.TransactionController.create_new_transaction(self, new_t_id, item.item_id, item.owner_id, session.user_id, p, rental_start_date, rental_end_date)

                # add two new jobs for transaction (for delivery and pickup)
                ctrl.JobsController.create_new_job(self, new_t_id)

                # set up next trans_id
                ctrl.JobsController.create_new_job(self, new_t_id)

                # allocate jobs automatically
                ctrl.JobsController.allocate_unallocated_jobs(ctrl.JobsController)


        def proceed_to_payment():
            u_controller = ctrl.UserController
            cardnum = u_controller.get_user_cardlongnumber_from_userid(session.user_id)
            cardate = u_controller.get_user_cardexpirydate_from_userid(session.user_id)
            cvv = u_controller.get_user_cardcvv_from_userid(session.user_id)
            nname = u_controller.get_user_name_from_userid(session.user_id)
            for widget in item_area.winfo_children():
                widget.destroy()
            number = Label(item_area, text='Card number', bg='#D27096', fg='white', relief=GROOVE)
            number.place(relx=0.40, rely=0.05, relwidth=0.27, relheight=0.05)
            T0 = Text(item_area, relief=GROOVE)
            T0.place(relx=0.40, rely=0.13, relwidth=0.27, relheight=0.05)
            T0.insert(END, cardnum)
            name = Label(item_area, text="Cardholder's name", bg='#D27096', fg='white', relief=GROOVE)
            name.place(relx=0.40, rely=0.25, relwidth=0.27, relheight=0.05)
            T2 = Text(item_area, relief=GROOVE)
            T2.place(relx=0.40, rely=0.33, relwidth=0.27, relheight=0.05)
            T2.insert(END, nname)
            date = Label(item_area, text="Expiry date", bg='#D27096', fg='white', relief=GROOVE)
            date.place(relx=0.40, rely=0.45, relwidth=0.27, relheight=0.05)
            T3 = Text(item_area, relief=GROOVE)
            T3.place(relx=0.40, rely=0.53, relwidth=0.27, relheight=0.05)
            T3.insert(END, cardate)
            code = Label(item_area, text="Security code", bg='#D27096', fg='white', relief=GROOVE)
            code.place(relx=0.40, rely=0.65, relwidth=0.27, relheight=0.05)
            T4 = Text(item_area, relief=GROOVE)
            T4.place(relx=0.40, rely=0.73, relwidth=0.27, relheight=0.05)
            T4.insert(END, cvv)
            pay = Button(item_area, text='Pay', bg='green', fg='white', relief=GROOVE, command=lambda: buy_another())
            pay.place(relx=0.49, rely=0.82, relwidth=0.1, relheight=0.1)

        def buy_another():
            for widget in item_area.winfo_children():
                widget.destroy()
            a1 = Label(item_area, text='Payment has been processed', bg='white', fg='black', relief=GROOVE, width=15)
            a1.place(relx=0.4, rely=0.5, relwidth=0.27, relheight=0.15)
            a2 = Button(item_area, text='Leave', bg='green', fg='white', relief=GROOVE, command=lambda: self.back_to_items(return_to_items, last_items_displayed, item_area,
                                                                    a1, a2, a3, a4, e1, e2, Rent, Calc, T))
            a2.place(relx=0.49, rely=0.82, relwidth=0.1, relheight=0.1)

        T = Text(screen, height=2, width=30, bg='red', fg='black', relief=GROOVE, )
        a1 = Label(screen, text='Beginning', bg='#D27096', fg='white', relief=GROOVE, width=10)
        a1.place(relx=0.52, rely=0.55, relwidth=0.07, relheight=0.05)
        date_var1 = StringVar(screen)
        date_var1.set(strdate)
        e1 = Entry(screen, textvariable=date_var1, bg='#D7EDF3', fg='blue', justify='center')
        e1.place(relx=0.59, rely=0.55, relwidth=0.1, relheight=0.05)
        a2 = Label(screen, text='Ending', bg='#D27096', fg='white', relief=GROOVE, width=10)
        a2.place(relx=0.52, rely=0.61, relwidth=0.07, relheight=0.05)
        date_var2 = StringVar(screen)
        date_var2.set(strdate1)
        print(strdate)
        print(strdate1)
        e2 = Entry(screen, textvariable=date_var2, bg='#D7EDF3', fg='blue', justify='center')
        e2.place(relx=0.59, rely=0.61, relwidth=0.1, relheight=0.05)
        a3 = Button(screen, text='Pick Date', bg='#70bed2', relief=GROOVE, command=lambda: fnCalendar(date_var1))
        a3.place(relx=0.69, rely=0.55, relwidth=0.07, relheight=0.05)
        a4 = Button(screen, text='Pick Date', bg='#70bed2', relief=GROOVE, command=lambda: fnCalendar(date_var2))
        a4.place(relx=0.69, rely=0.61, relwidth=0.07, relheight=0.05)
        Rent = Button(screen, text='Rent', bg='green', fg='white', relief=GROOVE,
                      command=lambda: order_summary(a1, a2, a3, a4, e1, e2, Rent, Calc, T))
        Rent.place(relx=0.64, rely=0.69, relwidth=0.07, relheight=0.05)
        Calc = Button(screen, text='Calculate price', bg='green', fg='white', relief=GROOVE,
                      command=lambda: calc())
        Calc.place(relx=0.64, rely=0.79, relwidth=0.10, relheight=0.05)

        class tkCalendar:
            def __init__(self, master, arg_year, arg_month, arg_day, arg_parent_updatable_var):
                self.update_var = arg_parent_updatable_var
                top = self.top = tk.Toplevel(master)
                try:
                    self.intmonth = int(arg_month)
                except:
                    self.intmonth = int(1)
                self.canvas = tk.Canvas(top, width=200, height=220, relief=tk.RIDGE, background="white",
                                        borderwidth=1)
                stryear = str(arg_year)

                self.year_var = tk.StringVar()
                self.year_var.set(stryear)
                self.lblYear = tk.Label(top, textvariable=self.year_var, font=fnta, background="white")
                self.lblYear.place(x=85, y=30)

                self.month_var = tk.StringVar()
                strnummonth = str(self.intmonth)
                strmonth = strnummonth
                self.month_var.set(strmonth)

                self.lblYear = tk.Label(top, textvariable=self.month_var, font=fnta, background="white")
                self.lblYear.place(x=85, y=50)
                # Variable muy usada
                tagBaseButton = "Arrow"
                self.tagBaseNumber = "DayButton"
                # draw year arrows
                x, y = 40, 43
                tagThisButton = "leftyear"
                tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
                self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
                x, y = 150, 43
                tagThisButton = "rightyear"
                tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
                self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
                # draw month arrows
                x, y = 40, 63
                tagThisButton = "leftmonth"
                tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
                self.fnCreateLeftArrow(self.canvas, x, y, tagFinalThisButton)
                x, y = 150, 63
                tagThisButton = "rightmonth"
                tagFinalThisButton = tuple((tagBaseButton, tagThisButton))
                self.fnCreateRightArrow(self.canvas, x, y, tagFinalThisButton)
                # Print days
                self.canvas.create_text(100, 90, text=strdays, font=fnta)
                self.canvas.pack(expand=1, fill=tk.BOTH)
                self.canvas.tag_bind("Arrow", "<ButtonRelease-1>", self.fnClick)
                self.canvas.tag_bind("Arrow", "<Enter>", self.fnOnMouseOver)
                self.canvas.tag_bind("Arrow", "<Leave>", self.fnOnMouseOut)
                self.fnFillCalendar()

            def fnCreateRightArrow(self, canv, x, y, strtagname):
                canv.create_polygon(x, y,
                                    [[x + 0, y - 5], [x + 10, y - 5], [x + 10, y - 10], [x + 20, y + 0],
                                     [x + 10, y + 10],
                                     [x + 10, y + 5], [x + 0, y + 5]], tags=strtagname, fill="blue", width=0)

            def fnCreateLeftArrow(self, canv, x, y, strtagname):
                canv.create_polygon(x, y,
                                    [[x + 10, y - 10], [x + 10, y - 5], [x + 20, y - 5], [x + 20, y + 5],
                                     [x + 10, y + 5],
                                     [x + 10, y + 10]], tags=strtagname, fill="blue", width=0)

            def fnClick(self, event):
                owntags = self.canvas.gettags(tk.CURRENT)
                if "rightyear" in owntags:
                    intyear = int(self.year_var.get())
                    intyear += 1
                    stryear = str(intyear)
                    self.year_var.set(stryear)
                if "leftyear" in owntags:
                    intyear = int(self.year_var.get())
                    intyear -= 1
                    stryear = str(intyear)
                    self.year_var.set(stryear)
                if "rightmonth" in owntags:
                    if self.intmonth < 12:
                        self.intmonth += 1
                        strnummonth = str(self.intmonth)
                        strmonth = strnummonth
                        self.month_var.set(strmonth)
                    else:
                        self.intmonth = 1
                        strnummonth = str(self.intmonth)
                        strmonth = strnummonth
                        self.month_var.set(strmonth)
                        intyear = int(self.year_var.get())
                        intyear += 1
                        stryear = str(intyear)
                        self.year_var.set(stryear)
                if "leftmonth" in owntags:
                    if self.intmonth > 1:
                        self.intmonth -= 1
                        strnummonth = str(self.intmonth)
                        strmonth = strnummonth
                        self.month_var.set(strmonth)
                    else:
                        self.intmonth = 12
                        strnummonth = str(self.intmonth)
                        strmonth = strnummonth
                        self.month_var.set(strmonth)
                        intyear = int(self.year_var.get())
                        intyear -= 1
                        stryear = str(intyear)
                        self.year_var.set(stryear)
                self.fnFillCalendar()

            def fnFillCalendar(self):
                init_x_pos = 20
                arr_y_pos = [110, 130, 150, 170, 190, 210]
                intposarr = 0
                self.canvas.delete("DayButton")
                self.canvas.update()
                intyear = int(self.year_var.get())
                monthcal = calendar.monthcalendar(intyear, self.intmonth)
                for row in monthcal:
                    xpos = init_x_pos
                    ypos = arr_y_pos[intposarr]
                    for item in row:
                        stritem = str(item)
                        if stritem == "0":
                            xpos += 27
                        else:
                            tagNumber = tuple((self.tagBaseNumber, stritem))
                            self.canvas.create_text(xpos, ypos, text=stritem, font=fnta, tags=tagNumber)
                            xpos += 27
                    intposarr += 1
                self.canvas.tag_bind("DayButton", "<ButtonRelease-1>", self.fnClickNumber)
                self.canvas.tag_bind("DayButton", "<Enter>", self.fnOnMouseOver)
                self.canvas.tag_bind("DayButton", "<Leave>", self.fnOnMouseOut)

            def fnClickNumber(self, event):
                owntags = self.canvas.gettags(tk.CURRENT)
                for x in owntags:
                    if x not in ("current", "DayButton"):
                        strdate = (str(self.year_var.get()) + "/" + str(self.month_var.get()) + "/" + str(x))
                        self.update_var.set(strdate)
                        self.top.withdraw()

            def fnOnMouseOver(self, event):
                self.canvas.move(tk.CURRENT, 1, 1)
                self.canvas.update()

            def fnOnMouseOut(self, event):
                self.canvas.move(tk.CURRENT, -1, -1)
                self.canvas.update()

    def render(self):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # ------- LEFT SIDE FILTER ------------
        left_bar = Frame(screen, bg='#70bed2')
        left_bar.place(relwidth=0.2, relheight=1)

        global show_availible
        show_availible = StringVar()

        item_availible = Checkbutton(left_bar, text="Show Only Availible", variable=show_availible)
        item_availible.place(relx=0.2, rely=0.2)

        # -------------------------------------

        # ---------- SEARCH BAR ---------------
        top_bar = Frame(screen, bg='#0f6185')
        top_bar.place(relx=0.2, rely=0, relwidth=0.8, relheight=0.1)

        # Search Prompt
        search_text = Label(screen, bg='#0f6185', text="Search:", fg='white', font=50)
        search_text.place(relx=0.22, rely=0, relheight=0.1)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(screen, text="Exit", command=lambda: main_nav_view.render())
        button.place(relx=0.7, rely=0, relheight=0.1)

        # Scrollbar
        scroll_bar = Scrollbar(screen)
        scroll_bar.pack(side=RIGHT, fill=Y)

        # ItemArea
        item_area = Frame(screen)
        item_area.place(relx=0.2, rely=0.1, relheight=0.9, relwidth=0.8)

        # Entry Field
        entry_field = Entry(screen, bg='#0f6185', fg='white', font=50)
        entry_field.place(relx=0.3, rely=0.01, relheight=0.08)
        screen.bind('<Return>', lambda x: self.display_queried_items(item_area, entry_field.get()))

        self.display_all_items(item_area)

        # --------------------------------------
        screen.mainloop()


# VIEW JOBS SCREEN ---------------------
class ViewJobs:
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen

    def view_job(self, event, job):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        #--------Get transaction ID
        t_controller = ctrl.TransactionController
        transaction = t_controller.get_transaction_from_id(job.transaction_id)

        #---------Get Item from item_id in Transaction
        i_controller = ctrl.ItemController
        item = i_controller.get_item_by_id(i_controller.get_item_by_id,transaction.item_id)

        #---------Get Renter information from renter in Transaction
        u_controller = ctrl.UserController
        renter = u_controller.get_user_from_userid(transaction.renter_id)

        time_rented = (transaction.endDate - transaction.startDate)
        price = time_rented.days * item.cost_per_day

        #Render information to screen
        Label(screen, text='Job ID: '+str(job.job_id)).pack()
        Label(screen, text='Item: '+item.title).pack()
        Label(screen, text='Renter: '+renter.name).pack()
        Label(screen, text='Start Date: '+str(transaction.startDate)).pack()
        Label(screen, text='End Date: '+str(transaction.endDate)).pack()
        Label(screen, text='Price: '+str(price)).pack()
        #Checkbox to mark job as complete



        if job.delivered_datetime == None:
            complete = Button(screen, text="Complete Job", width=10, height=1, command=lambda: self.complete_job(job))
            complete.pack()
        else:
            Label(screen, text=job.delivered_datetime.strftime('%Y-%m-%d %H:%M:%S')).pack()

        go_back = Button(screen, text="Go Back", width=10, height=1, command=lambda: self.render())
        go_back.pack()


    def complete_job(self, job):
        j_controller = ctrl.JobsController
        if j_controller.complete_job(job):
            # if successful re-render jobpage
            self.view_job(self,job)
        else:
            return False

    def render(self):

        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        nrows = 2
        job_controller = ctrl.JobsController
        staff_joblist = job_controller.get_joblist_from_staff_id(session.user_id)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(screen, text="Go Back", command=lambda: main_nav_view.render())
        button.grid(row=1, column=1, pady=4)

        if staff_joblist == None:
            Label(screen, text='Jobs allocated to ' + str(session.user_id)).pack()
            Label(screen, text='No current jobs').grid(row=1, column=2).pack()

        else:
            staff_jobs = job_controller.get_jobs_from_joblist(staff_joblist.job_list_id)

            Label(screen, text='Jobs allocated to ' + str(session.user_id)).grid(row=0, column=2)
            Label(screen, text="Job_ID").grid(row=1, column=3)
            Label(screen, text="JobList_ID").grid(row=1, column=4)
            Label(screen, text="Delivered_Time").grid(row=1, column=5)

            for j in staff_jobs:
                jobid = Label(screen, text=j.job_id)
                jobid.bind('<Button-1>', partial(self.view_job, job = j))
                jobid.grid(row=nrows, column=3)
                Label(screen, text=j.job_list_id).grid(row=nrows, column=4)
                if j.delivered_datetime == None:
                    Label(screen, text='Not Complete').grid(row=nrows, column=5)
                else:
                    Label(screen, text=j.delivered_datetime.strftime('%Y-%m-%d %H:%M:%S')).grid(row=nrows, column=5)

                nrows += nrows

# VIEW TRANSACTIONS
class ViewTransactions:
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen

    def display_queried_orders(self, area, query):
        if query is "":
            return self.display_all_orders(area)
        t_controller = ctrl.TransactionController
        my_transactions = t_controller.get_transactions_from_query(query, session.user_id)

        print(my_transactions)
        self.display_orders(area, my_transactions)

    def display_all_orders(self, area):
        t_controller = ctrl.TransactionController
        my_transactions = t_controller.get_transactions_from_userid(session.user_id)

        self.display_orders(area, my_transactions)

    def send_review(self, transaction_id, content, rating, mode): #needs to take rating
        rev_controller = ctrl.ReviewController
        if mode == 0:
            rev_controller.create_new_review(rev_controller,transaction_id,content,rating)
        else:
            rev_controller.edit_review(rev_controller,transaction_id,content,rating)

        self.render()


    def leave_review(self, area, transaction_id, order):
        for widget in area.winfo_children():
            widget.destroy()

        Label(area, text=("Item: "+order[0])).pack()
        Label(area, text=("From: "+str(order[1]))).pack()
        Label(area, text=("To: "+str(order[2]))).pack()
        Label(area, text=("Owner: "+order[3])).pack()

        ratings = {1,2,3,4,5}
        rating = IntVar()

        rating_dropdown = OptionMenu(area, rating, *ratings)
        rating_dropdown.pack()

        cont = Text(area, width=40, height=5)

        if not ctrl.TransactionController.has_review(transaction_id):
            rating.set(3)
            Label(area, text="Leave Review:", pady=20).pack()
            cont.pack()
            Button(area, text="Submit", command=lambda: self.send_review(transaction_id, cont.get("1.0", END), rating.get(), 0)).pack()
        else:
            rev_controller = ctrl.ReviewController
            cont.insert(INSERT, rev_controller.get_review_from_transaction_id(rev_controller,transaction_id).content)
            cont.pack()
            rating.set(rev_controller.get_review_from_transaction_id(rev_controller,transaction_id).rating)
            Button(area, text="Update", command=lambda: self.send_review(transaction_id, cont.get("1.0", END),rating.get(), 1)).pack()


    def display_orders(self, area, orders):
        for widget in area.winfo_children():
            widget.destroy()
        Label(area, text="Item").grid(row=0, column=1)
        Label(area, text="Start Date").grid(row=0, column=2)
        Label(area, text="End Date").grid(row=0, column=3)
        Label(area, text="Price").grid(row=0, column=4)
        r = 1
        t_controller = ctrl.TransactionController
        for order in orders:
            print(order)
            order_details = t_controller.order_details_from_transaction(order)

            area.rowconfigure(r, weight=2)

            image = Label(area, text="IMAGE", bg="green")
            image.grid(row=r, column=0, padx=(50, 50))

            title = Label(area, text=order_details[0])
            title.grid(row=r, column=1, padx=(50, 50))
            title.configure(font="30")

            rented_from = Label(area, text=order_details[1])
            rented_from.grid(row=r, column=2, padx=(30, 30))
            rented_from.configure(font="30")

            rented_until = Label(area, text=order_details[2])
            rented_until.grid(row=r, column=3, padx=(30, 30))
            rented_until.configure(font="30")

            price = Label(area, text=order_details[4])
            price.grid(row=r, column=4, padx=(30, 30))
            price.configure(font="30")

            if ctrl.TransactionController.has_review(order.transaction_id):
                feedback = Button(area, text="Edit Review",
                                  command=partial(self.leave_review, area=area, transaction_id=order.transaction_id,
                                                  order=order_details))
                feedback.grid(row=r, column=5)
            else:
                feedback = Button(area, text="Leave Review",
                                  command= partial(self.leave_review,area =area, transaction_id = order.transaction_id,
                                                   order = order_details))
                feedback.grid(row=r, column=5)
            r += 1

    def render(self):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # ---------- SEARCH BAR ---------------
        top_bar = Frame(screen, bg='#0f6185')
        top_bar.place(relx=0.2, rely=0, relwidth=0.8, relheight=0.1)

        # Search Prompt
        search_text = Label(screen, bg='#0f6185', text="Search:", fg='white', font=50)
        search_text.place(relx=0, rely=0, relheight=0.1, relwidth=0.3)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(screen, text="Exit", command=lambda: main_nav_view.render())
        button.place(relx=0.7, rely=0, relheight=0.1)

        # ItemArea
        order_area = Frame(screen)
        order_area.place(relx=0, rely=0.1, relheight=0.9, relwidth=1)

        # Entry Field
        entry_field = Entry(screen, bg='#0f6185', fg='white', font=50)
        entry_field.place(relx=0.3, rely=0.01, relheight=0.08)
        screen.bind('<Return>', lambda x: self.display_queried_orders(order_area, entry_field.get()))

        self.display_all_orders(order_area)

        # --------------------------------------
        screen.mainloop()


# ADD STAFF VIEW
class ViewAddStaff:
    def __init__(self, screen):
        # Stores a reference to the screen
        self.screen = screen

        self.full_name = StringVar()
        self.region = StringVar()
        self.username = StringVar()
        self.email = StringVar()
        self.password = StringVar()
        self.capacity = IntVar()
        self.capacity.set(10)

    def register_staff(self):

        # Check for empty fields
        if self.full_name.get() == "":
            self.render("Full name is required", "")
            return False
        if self.email.get() == "":
            self.render("Email address is required", "")
            return False
        if self.region.get() == "":
            self.render("Region is required", "")
            return False
        if self.username.get() == "":
            self.render("Username is required", "")
            return False
        if self.password.get() == "":
            self.render("Password is required", "")
            return False
        if self.capacity.get() == None:
            self.render("Capacity is required.", "")
            return False

        print("Registering...")
        # check email format
        if ctrl.UserController.validate_email_format(self.email.get()):
            # Check email is unique
            if ctrl.UserController.validate_email(self.email.get()):
                print("Email is unique")

                # Check username is unique
                if ctrl.UserController.validate_user_name(self.username.get()):
                    print("username is unique")

                    if ctrl.RegisterStaffController.register_staff(self.full_name.get(),
                                                                   self.region.get(),
                                                                   self.email.get(),
                                                                   self.username.get(),
                                                                   self.password.get(),
                                                                   self.capacity.get()):
                        print("Added successfully")
                        self.render("", self.username.get() + " added successfully")
                    else:
                        print("Error adding new staff member")
                        self.render("Error adding new staff member", "")
                else:
                    print("Username taken")
                    self.render("username taken", "")
            else:
                print("Email already in use")
                self.render("Email already in use", "")
        else:
            print("Invalid email format")
            self.render("Please enter a valid email address", "")

    def nav_to_main_nav(self):
        main_nav = MainNavView(screen)
        main_nav.render()

    def render(self, err_message, success_message):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        test = Label(screen, text="Add New Staff", bg="grey", width="300", height="2", font=("calibri", 13))
        test.pack()

        # user_id, name, username, password_hash, email, region, staff_id):

        # SCREEN LAYOUT
        Label(screen, text="Please enter details below", bg="grey", width="300", height="2",
              font=("Calibri", 13)).pack()
        Label(screen, text="").pack()

        Label(screen, text="Full name *").pack()
        staff_full_name_entry = Entry(screen, textvariable=self.full_name)
        staff_full_name_entry.pack()

        Label(screen, text="Email *").pack()
        email_entry = Entry(screen, textvariable=self.email)
        email_entry.pack()

        Label(screen, text="Region *").pack()
        region_entry = Entry(screen, textvariable=self.region)
        region_entry.pack()

        Label(screen, text="Username*").pack()
        username_entry = Entry(screen, textvariable=self.username)
        username_entry.pack()

        Label(screen, text="Password *").pack()
        password_entry = Entry(screen, textvariable=self.password)
        password_entry.pack()

        Label(screen, text="Capacity *").pack()
        cap_entry = Entry(screen, textvariable=self.capacity)
        cap_entry.pack()

        Button(screen, text="Register Staff", width=10, height=1, command=lambda: self.register_staff()).pack()
        Button(screen, text="Done", width=10, height=1, command=lambda: self.nav_to_main_nav()).pack()
        Label(screen, text=err_message, fg="red", font=("calibri", 11)).pack()
        Label(screen, text=success_message, fg="green", font=("calibri", 11)).pack()


# MAIN NAVIGATION WINDOW ---------------
class MainNavView:
    def __init__(self, screen):
        self.screen = screen

    def nav_to_view_items(self):
        view_items = ViewItems(self.screen)
        view_items.render()

    def nav_to_add_items(self):
        add_item_view = AddItemView(self.screen)
        add_item_view.render("")

    def nav_to_jobs_view(self):
        jobs_view = ViewJobs(self.screen)
        jobs_view.render()

    def log_out(self):
        log_in_view = LogInView(self.screen)
        log_in_view.render("")

    def nav_to_orders(self):
        order_view = ViewTransactions(self.screen)
        order_view.render()

    def nav_to_add_category(self):
        add_category_view = AddCategoryView(self.screen)
        add_category_view.render("")

    def nav_to_add_staff(self):
        add_staff_view = ViewAddStaff(self.screen)
        add_staff_view.render("", "")

    def nav_to_job_info(self):
        job_info = ViewJobsInfo(self.screen)
        job_info.render()

    def nav_to_manage_users(self):
        manage_users = ManageUsersView(self.screen)
        manage_users.render()

    def render(self):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        print("Got to login screen, session user_id is " + str(session.user_id))

        # SCREEN LAYOUT
        # !!! CHECK SESSION ACCESS BEFORE BUTTON IS RENDERED TO VIEW !!!
        Label(screen, text="Main menu", bg="grey", width="300", height="2", font=("calibri", 13)).pack()
        Label(screen, text="").pack()

        view_items = Button(screen, text="View Items", width=15, height=2, command=lambda: self.nav_to_view_items())
        view_items.pack()

        add_items = Button(screen, text="Add Item", width=15, height=2, command=lambda: self.nav_to_add_items())
        add_items.pack()

        view_transactions = Button(screen, text="View Orders", width=15, height=2, command=lambda: self.nav_to_orders())
        view_transactions.pack()

        # Staff log in
        if session.access == 1:
            Label(screen, text="").pack()
            view_jobs = Button(screen, text="View Jobs", width=15, height=2, command=lambda: self.nav_to_jobs_view())
            view_jobs.pack()


        # Admin log in
        if session.access == 2:
            Label(screen, text="").pack()
            add_staff = Button(screen, text="Add Staff", width=15, height=2, command=lambda: self.nav_to_add_staff())
            add_staff.pack()
            manage_users = Button(screen, text="Manage Users", width=15, height=2, command=lambda: self.nav_to_manage_users())
            manage_users.pack()
            view_job_info = Button(screen, text="Jobs Summary", width=15, height=2, command=lambda: self.nav_to_job_info())
            view_job_info.pack()
            add_category = Button(screen, text="New Category", width=15, height=2, command=lambda: self.nav_to_add_category())
            add_category.pack()

        Label(screen, text="").pack()
        logout_button = Button(screen, text="Log out", width=15, height=1, command=lambda: self.log_out())
        logout_button.pack()


# VIEW ITEMS SCREEN -------------------------

# VIEW JOBS INFORMATION SCREEN --------------
class ViewJobsInfo:
    def __init__(self, screen):
        self.screen = screen

    def display_all_regions(self, area):
        job_info_ctrl = ctrl.JobsInfoController
        job_info = job_info_ctrl.get_all_jobs_info(self)
        Label(area, text="# Of Delivery Jobs").grid(row=0, column=1)
        Label(area, text="# Completed").grid(row=0, column=2)
        Label(area, text="# Incomplete").grid(row=0, column=3)
        Label(area, text="% Outstanding").grid(row=0, column=4)

        total_jobs = Label(area, text=job_info[0])
        total_jobs.grid(row=1, column=1, padx=(30, 30))
        total_jobs.configure(font="15")

        comp_jobs = Label(area, text=job_info[1])
        comp_jobs.grid(row=1, column=2, padx=(30, 30))
        comp_jobs.configure(font="15")

        incomp_jobs = Label(area, text=job_info[2])
        incomp_jobs.grid(row=1, column=3, padx=(30, 30))
        incomp_jobs.configure(font="15")

        incomp_pct = Label(area, text=str(job_info[3]) + "%")
        incomp_pct.grid(row=1, column=4, padx=(30, 30))
        incomp_pct.configure(font="15")


    def display_queried_region(self, area, query):
        for widget in area.winfo_children():
            widget.destroy()

        if query is "":
            return self.display_all_regions(area)

        job_info_ctrl = ctrl.JobsInfoController
        job_info = job_info_ctrl.get_all_jobs_by_region_info(self, query)

        Label(area, text="# Of Delivery Jobs").grid(row=0, column=1)
        Label(area, text="# Completed").grid(row=0, column=2)
        Label(area, text="# Incomplete").grid(row=0, column=3)
        Label(area, text="% Outstanding").grid(row=0, column=4)

        total_jobs = Label(area, text=job_info[0])
        total_jobs.grid(row=1, column=1, padx=(30, 30))
        total_jobs.configure(font="15")

        comp_jobs = Label(area, text=job_info[1])
        comp_jobs.grid(row=1, column=2, padx=(30, 30))
        comp_jobs.configure(font="15")

        incomp_jobs = Label(area, text=job_info[2])
        incomp_jobs.grid(row=1, column=3, padx=(30, 30))
        incomp_jobs.configure(font="15")

        incomp_pct = Label(area, text=str(job_info[3]) + "%")
        incomp_pct.grid(row=1, column=4, padx=(30, 30))
        incomp_pct.configure(font="15")



    def render(self):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        # ---------- REGION SEARCH BAR ---------------
        top_bar = Frame(screen, bg='#0f6185')
        top_bar.place(relx=0.2, rely=0, relwidth=0.8, relheight=0.1)

        # Search Prompt
        search_text = Label(screen, bg='#0f6185', text="Region:", fg='white', font=50)
        search_text.place(relx=0, rely=0, relheight=0.1, relwidth=0.3)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(screen, text="Exit", command=lambda: main_nav_view.render())
        button.place(relx=0.7, rely=0, relheight=0.1)

        # ItemArea
        info_area = Frame(screen)
        info_area.place(relx=0, rely=0.1, relheight=0.9, relwidth=1)

        # Entry Field
        entry_field = Entry(screen, bg='#0f6185', fg='white', font=50)
        entry_field.place(relx=0.3, rely=0.01, relheight=0.08)
        screen.bind('<Return>', lambda x: self.display_queried_region(info_area, entry_field.get()))

        self.display_all_regions(info_area)

        # --------------------------------------
        screen.mainloop()


class AddCategoryView:
    def __init__(self,screen):
        self.screen = screen

        self.cat_title = StringVar()
        self.cat_desc = StringVar()

    def add_new_category(self):
        if len(self.cat_title.get()) == 0:
            self.render("Please Enter Category Name")
            return True
        cat_controller = ctrl.CategoryController
        print(self.cat_title.get(), self.cat_desc.get())
        if cat_controller.addCategory(cat_controller, self.cat_title.get(), self.cat_desc.get()):
            self.cat_title = ""
            self.cat_desc = ""
            self.render("Added Successfully")
        else:
            self.render("Category already exists")

    def display_categories(self, area):
        cat_controller = ctrl.CategoryController
        cats = cat_controller.getCategories(cat_controller)
        for cat in cats:
            Label(area, text=cat.title,bg="#E0E0E0").pack()

    def render(self, message):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        Label(screen, text="Add New Category", bg="grey", width="300", height="2", font=("calibri", 13)).pack()
        Label(screen, text="").pack()

        form_area = Frame(screen, bg="#E0E0E0")
        form_area.place(relx=0.1, rely=0.2, relwidth=0.3, relheight=0.7)
        existing_categories_area = Frame(screen, bg="#E0E0E0")
        existing_categories_area.place(relx=0.6, rely=0.2, relwidth=0.3, relheight=0.7)

        #Add Category Form
        header = Label(form_area, text="Add Category", bg="#E0E0E0", font=("",12), pady=40).pack()

        Label(form_area, text="Category: ", bg="#E0E0E0" ).pack()
        Entry(form_area, textvariable = self.cat_title).pack()

        Label(form_area, text="Description",bg="#E0E0E0").pack()
        Entry(form_area, textvariable = self.cat_desc).pack()

        if message == "Added Successfully":
            Label(form_area, text=message, fg="green", bg="#E0E0E0").pack()
        else:
            Label(form_area, text=message, fg="red", bg="#E0E0E0").pack()

        Label(form_area, bg="#E0E0E0", pady=20).pack()

        Button(form_area, text="Create", command=lambda:self.add_new_category()).pack()


        #Categories List

        self.display_categories(existing_categories_area)

        # back to main menu button
        main_nav_view = MainNavView(self.screen)
        button = Button(screen, text="Exit", command=lambda: main_nav_view.render())
        button.place(relx=0.7, rely=0, relheight=0.1)


class ManageUsersView():
    def __init__(self, screen):
        self.screen = screen

    def nav_to_main_nav(self):
        main_nav = MainNavView(screen)
        main_nav.render()

    def swap_status(self, customer):
        if customer.active:
            customer.active = False
        else:
            customer.active = True

        self.render()

    # displays all customers to 'area' and returns the grid row the final customer is printed to
    def display_customers(self, area):
        # get customers
        customers = ctrl.UserController.get_all_customers()

        id = Label(area, text="ID")
        id.grid(row=1, column=1, padx=(50, 50), pady=(0, 20))
        id.configure(font="30")

        username = Label(area, text="USERNAME")
        username.grid(row=1, column=2, padx=(30, 30),  pady=(0, 20))
        username.configure(font="30")

        email = Label(area, text="EMAIL")
        email.grid(row=1, column=3, padx=(30, 30), pady=(0, 20))
        email.configure(font="30")

        email = Label(area, text="TYPE")
        email.grid(row=1, column=4, padx=(30, 30), pady=(0, 20))
        email.configure(font="30")

        email = Label(area, text="STATUS")
        email.grid(row=1, column=5, padx=(30, 30), pady=(0, 20))
        email.configure(font="30")

        change_status = Label(area, text="CHANGE STATUS")
        change_status.grid(row=1, column=6, padx=(30, 30), pady=(0, 20))
        change_status.configure(font="30")

        r = 2
        for customer in customers:
            print("customer " + str(r) + " Status " + str(customer.active))
            id = Label(area, text=customer.user_id)
            id.grid(row=r, column=1, padx=(50, 50))
            id.configure(font="30")

            username = Label(area, text=customer.username)
            username.grid(row=r, column=2, padx=(30, 30))
            username.configure(font="30")

            email = Label(area, text=customer.email)
            email.grid(row=r, column=3, padx=(30, 30))
            email.configure(font="30")

            email = Label(area, text="Customer")
            email.grid(row=r, column=4, padx=(30, 30))
            email.configure(font="30")

            cust_status = "Active" if customer.active is True else "Inactive"

            active = Label(area, text=cust_status)
            active.grid(row=r, column=5, padx=(30, 30))
            active.configure(font="30")

            if customer.active is True:
                button = Button(area, text="Deactivate", command=partial(self.swap_status, customer=customer))
                button.grid(row=r, column=6, padx=(30, 30))
                button.configure(font="30")
            else:
                button = Button(area, text="Activate", command=partial(self.swap_status, customer=customer))
                button.grid(row=r, column=6, padx=(30, 30))
                button.configure(font="30")

            r += 1

        return r

    def display_staff(self, area, row):
        staff_list = ctrl.UserController.get_all_staff()

        r = row
        for staff in staff_list:
            print("customer " + str(r) + " Status " + str(staff.active))
            id = Label(area, text=staff.user_id)
            id.grid(row=r, column=1, padx=(50, 50))
            id.configure(font="30")

            username = Label(area, text=staff.username)
            username.grid(row=r, column=2, padx=(30, 30))
            username.configure(font="30")

            email = Label(area, text=staff.email)
            email.grid(row=r, column=3, padx=(30, 30))
            email.configure(font="30")

            email = Label(area, text="Staff")
            email.grid(row=r, column=4, padx=(30, 30))
            email.configure(font="30")

            staff_status = "Active" if staff.active is True else "Inactive"

            active = Label(area, text=staff_status)
            active.grid(row=r, column=5, padx=(30, 30))
            active.configure(font="30")

            if staff.active is True:
                button = Button(area, text="Deactivate", command=partial(self.swap_status, customer=staff))
                button.grid(row=r, column=6, padx=(30, 30))
                button.configure(font="30")
            else:
                button = Button(area, text="Activate", command=partial(self.swap_status, customer=staff))
                button.grid(row=r, column=6, padx=(30, 30))
                button.configure(font="30")

            r += 1

        return r

    def render(self):
        # Clear screen, redraw new screen
        for widget in screen.winfo_children():
            widget.destroy()

        Label(screen, text="Manage Users", bg="grey", width="300", height="2", font=("calibri", 13)).pack()

        # User area
        user_area = Frame(screen)
        user_area.place(relx=0, rely=0.1, relheight=0.9, relwidth=1)

        # List all customers
        row = self.display_customers(user_area)

        # List all staff
        row = self.display_staff(user_area, row)

        Button(user_area, text="Done", width=10, height=1, command=lambda: self.nav_to_main_nav()).grid(row=row, column=6, padx=(30, 30), pady=(30, 0))
# MAIN -----------------
# Initialise main screen
screen = Tk()
screen.geometry("1400x700")
screen.title("Rental Management System")

# Fill jobs
ctrl.JobsController.allocate_unallocated_jobs(ctrl.JobsController)

# Initialise session object
session = ctrl.Session()

# Initialise views
log_in_view = LogInView(screen)

# RUN -------------------
log_in_view.render("")
