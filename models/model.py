# CMP-5012B coursework - Delivery Management System Prototype
#
# Authors:      Matthew Taylor, Kaloyan Valchev, George Marshall-Dutton, Jez Bayliss, Maz Gudelis
#
# Description:  Model view - holds classes to prototype database design, stores all data for the system

import datetime


# set up classes
class Transaction:
    def __init__(self, transaction_id, item_id, owner, renter, total_cost, start_date, end_date):
        self.transaction_id = transaction_id
        self.item_id = item_id
        self.owner_id = owner
        self.renter_id = renter
        self.totalCost = total_cost
        self.startDate = start_date
        self.endDate = end_date


class Item:
    autoID = 0
    def __init__(self, title, cost_per_day, owner_id, category_id, available, region, info):
        self.item_id = Item.autoID
        self.title = title
        self.cost_per_day = cost_per_day
        self.owner_id = owner_id
        self.category_id = category_id
        self.available = available
        self.region = region
        self.info = info

        Item.autoID += 1

class Category:
    autoID = 0
    def __init__(self, title, description):
        self.category_id = Category.autoID
        self.title = title
        self.description = description

        Category.autoID += 1


class User:
    autoID = 0

    def __init__(self, name, username, password_hash, email, region):
        self.user_id = User.autoID
        self.name = name
        self.username = username
        self.password_hash = password_hash
        self.email = email
        self.region = region
        self.active = True

        User.autoID += 1


class Customer(User):
    def __init__(self, name, username, password_hash, email, region, card_long_number, card_expiry_date, card_cvv):
        super().__init__( name, username, password_hash, email, region)
        self.cardLongNumber = card_long_number
        self.cardExpiryDate = card_expiry_date
        self.cardCVV = card_cvv



class Staff(User):
    def __init__(self, name, username, password_hash, email, region, cap):
        super().__init__( name, username, password_hash, email, region)
        self.cap = cap


class Admin(User):
    def __init__(self, name, username, password_hash, email, region, admin_id):
        super().__init__( name, username, password_hash, email, region)
        self.admin_id = admin_id

class Payment:
    def __init__(self, transaction_id, user_id, payment_amt, payment_date):
        self.transaction_id = transaction_id
        self.user_id = user_id
        self.paymentAmt = payment_amt
        self.paymentDate = payment_date

class Dispute:
    def __init__(self, transaction_id, content, resolved):
        self.transaction_id = transaction_id
        self.content = content
        self.resolved = resolved


class Review:
    def __init__(self, transaction_id, content, rating):
        self.transaction_id = transaction_id
        self.content = content
        self.rating = rating


class Joblist:
    autoID = 0
    def __init__(self, staff_id):
        self.job_list_id = Joblist.autoID
        self.staff_id = staff_id
        Joblist.autoID += 1

class Job:
    def __init__(self, job_id, transaction_id, job_list_id, delivered_datetime):
        self.job_id = job_id
        self.transaction_id = transaction_id
        self.job_list_id = job_list_id
        self.delivered_datetime = delivered_datetime


# create test data

# transaction setup
#                   transaction_id, item_id,    owner_id,   renter_id,  total_cost,     start_date,                 end_date
t1 = Transaction(   1,              0,          1,          1,          20,             datetime.date(2019, 2, 15), datetime.date(2019, 2, 20))
t2 = Transaction(   2,              1,          0,          2,          10,             datetime.date(2019, 2, 17), datetime.date(2019, 2, 18))
t3 = Transaction(   3,              2,          1,          2,          10,             datetime.date(2019, 1, 25), datetime.date(2019, 2, 2))
t4 = Transaction(   4,              3,          2,          2,          10,             datetime.date(2019, 1, 15), datetime.date(2019, 2, 19))
t5 = Transaction(   5,              2,          4,          2,          10,             datetime.date(2019, 2, 15), datetime.date(2019, 2, 17))
t6 = Transaction(   6,              1,          2,          3,          10,             datetime.date(2019, 1, 9),  datetime.date(2019, 1, 19))
t7 = Transaction(   7,              0,          1,          3,          10,             datetime.date(2019, 1, 3),  datetime.date(2019, 1, 5))
t8 = Transaction(   8,              1,          0,          5,          10,             datetime.date(2019, 1, 11), datetime.date(2019, 1, 12))
t9 = Transaction(   9,              3,          1,          4,          10,             datetime.date(2019, 1, 1),  datetime.date(2019, 1, 9))
t10 = Transaction(  10,             4,          0,          2,          10,             datetime.date(2019, 1, 19), datetime.date(2019, 2, 25))

# holds list of transactions
transactions = [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10]

# category setup
cat1 = Category("TV", "LED, OLED, PDP, Flatscreen etc...")
cat2 = Category("Monitors", "LCD, 4k, touchscreen etc...")
cat3 = Category("Projectors", "DLP, LCD, Screens, Bulbs etc...")
cat4 = Category("Games Consoles", "Nintendo consoles, Playstations, Xboxes, retro etc...")
categories = [cat1,cat2,cat3,cat4]

# item setup
#         title,                    cost_per_day,   owner_id,   category_id,   available,  region,             info):
i1 = Item('Panasonic 40" LED',          15,             1,          0,          True,       'Norfolk',          'Really affordable and very loud')
i2 = Item('Samsung 52" QLED',           35,             1,          0,          False,      'Norfolk',          'Great for large group viewings')
i3 = Item('50" Android Smart Ultra HD', 30,             2,          0,          False,      'Cambridgeshire',   'Easy to connect with smartphones')
i4 = Item('Artograph LED EZ Tracer',    12,             0,          2,          True,       'Norfolk',          'Best at distance of 4ft')
i5 = Item('Optoma X402',                18,             3,          2,          True,       'Cambridgeshire',   'great for outdoor events wide view best at')
i6 = Item('Xbox 360 with 2 controllers', 20,            3,          3,          True,       'Cambridgeshire',   'comes with fifa 18')
i7 = Item('Xbox 360, 4 wireless controllers',30,        0,          3,          True,       'Norfolk',          'HDMI cable included, controllers need batteries')
i8 = Item('Nintendo switch, dock and 2 joycons',18,     0,          3,          True,       'Norfolk',          'Has mario kart and mario party')

# holds list of items
items = [i1, i2, i3, i4, i5, i6, i7, i8]


# set up customer records
# name, username, password_hash, email, region, card_long_number, card_expiry_date, card_cvv
c1 = Customer('Matthew Taylor', 'mme16gnu', 'PASSWORD', 'm.taylor2@uea.ac.uk',      'Norfolk',        1986332574126502, datetime.date(2020, 2, 1),  123)
c2 = Customer('Kal Valchev',    'kally',    'KALPASS',  'kal@uea.ac.uk',            'Suffolk',        1256863201493502, datetime.date(2021, 6, 1),  670)
c3 = Customer('Maz',            'mazzy',    'password', 'mazvidaZZZ@gmail.com',     'Cambridgeshire', 8541223684510265, datetime.date(2021, 10, 1), 325)
c4 = Customer('Kevin',          'kman',     'password', 'bigkman@hotmail.com',      'Cambridgeshire', 1256987402236100, datetime.date(2022, 3, 1),  661)
c5 = Customer('Sally Smith',    'ssmith89', 'password', 'sillysally89@yahoo.com',   'Suffolk',        1459865123699670, datetime.date(2019, 12, 1), 303)

c2.active = False

# holds list of customers
customers = [c1, c2, c3, c4, c5]


# set up staff records
# name, username, password_hash, email, region, staff_id,cap):
s1 = Staff('Jez Bayliss',   'bayliss',  'JEZPASS',  'jez.bayliss@uea.ac.uk',    'Cambridgeshire',   10)
s2 = Staff('Mr DPD',        'dpd',      'DPDPASS',  'dpd@dpd.com',              'Norfolk',          10)
s3 = Staff('Del Iver',      'del',      'pass',     'awd@dpd.com',              'Norfolk',          10)
s4 = Staff('Viktor V',      'vikv',     'password', 'vik@gmail.com',            'Suffolk',          10)

# holds list of staff
stafflist = [s1, s2, s3, s4]

# set up admin records
a1 = Admin('Kelly Anne',    'admin',        'password', 'kellyanne@onceborrowed.co.uk', 'London',   'BOS099')
a2 = Admin('Ant Admin',     'ant-admin',    'password', 'ant@onceborrowed.co.uk',       'Norfolk',  'NOR001')

# holds list of admins
admins = [a1, a2]

# payment setup
p1 = Payment(1, 2, 20, datetime.date(2019, 2, 15)) # pays off transaction t1
payments = [p1]

# review setup
# transaction_id, content, rating
r1 = Review(2,'Great pair of shoes', 5)
reviews = [r1]


# joblist setup (stores lists of jobs per staff member
#            staff_id
jlist1 = Joblist(5)  # jez's joblist
jlist2 = Joblist(6)  # mr dpd's joblist
jlist3 = Joblist(7)  # mr dpd's joblist
jobLists = [jlist1, jlist2, jlist3]

# job setup
#job_id, transaction_id, job_list_id, delivered_datetime
j1 = Job(1, 1, 1, datetime.datetime(2019, 2, 20, 16, 30))   # delivered 20/02/19 @ 16:30
j2 = Job(2, 2, 1, None)
j3 = Job(3, 3, 1, datetime.datetime(2019, 2, 20, 16, 30))
j4 = Job(4, 4, 2, datetime.datetime(2019, 2, 20, 16, 30))
j5 = Job(5, 5, 2, datetime.datetime(2019, 2, 20, 16, 30))
j6 = Job(6, 6, 3, datetime.datetime(2019, 2, 20, 16, 30))
j7 = Job(7, 7, 3, datetime.datetime(2019, 2, 20, 16, 30))
j8 = Job(8, 8, 2, datetime.datetime(2019, 2, 20, 16, 30))
j9 = Job(9, 9, 2, datetime.datetime(2019, 2, 20, 16, 30))
j10 = Job(10, 10, 1, datetime.datetime(2019, 2, 20, 16, 30))
#Jobs without job_list_id, to allocate to drivers joblists
j11 = Job(11, 1, None, None)
j12 = Job(12, 2, None, None)
j13 = Job(13, 3, None, None)
j14 = Job(14, 4, None, None)
j15 = Job(15, 5, None, None)
j16 = Job(16, 6, None, None)
j17 = Job(17, 7, None, None)
j18 = Job(18, 8, None, None)
j19 = Job(19, 9, None, None)
j20 = Job(20, 10, None, None)
j21 = Job(21, 10, None, None)
j22 = Job(22, 10, None, None)
j23 = Job(23, 10, None, None)
jobs = ([j1, j2, j3, j4, j5, j6, j7, j8, j9, j10,
        j11, j12, j13, j14, j15, j16, j17, j18, j19, j20, j21, j22, j23])
